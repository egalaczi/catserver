define(function (Person, utils) {
  var people = [];
 
  people.push(new Person('Jim'));
  people.push(new Person(utils.someValue));
 
 
  return {people: people};
});
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
import rest_framework
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from cats.views import CatView, CatDetailView, ActionView
from todo.views import TodoDetailView, TodoView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'skeleton.views.home', name='home'),
    # url(r'^skeleton/', include('skeleton.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name="todo.html")),
    url(r'^todos/$', TodoView.as_view(), name='todo-view'),
    url(r'^todos/(?P<pk>\d+)$', TodoDetailView.as_view(), name='todo-view'),
    url(r'^cats/action/(?P<pk>\d+)/(?P<action>\w+)/$', ActionView.as_view(), name='cat-view'),
    url(r'^cats/$', CatView.as_view(), name='cat-view'),
    url(r'^cats/(?P<pk>\d+)/$', CatDetailView.as_view(), name='cat-detail'),
    url(r'^upload/$','cats.views.upload'),
)

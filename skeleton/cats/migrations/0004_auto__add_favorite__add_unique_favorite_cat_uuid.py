# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Favorite'
        db.create_table('cats_favorite', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cats.Cat'])),
            ('uuid', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('cats', ['Favorite'])

        # Adding unique constraint on 'Favorite', fields ['cat', 'uuid']
        db.create_unique('cats_favorite', ['cat_id', 'uuid'])


    def backwards(self, orm):
        # Removing unique constraint on 'Favorite', fields ['cat', 'uuid']
        db.delete_unique('cats_favorite', ['cat_id', 'uuid'])

        # Deleting model 'Favorite'
        db.delete_table('cats_favorite')


    models = {
        'cats.cat': {
            'Meta': {'object_name': 'Cat'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.TextField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'cats.favorite': {
            'Meta': {'unique_together': "(('cat', 'uuid'),)", 'object_name': 'Favorite'},
            'cat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cats.Cat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'cats.like': {
            'Meta': {'unique_together': "(('cat', 'uuid'),)", 'object_name': 'Like'},
            'cat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cats.Cat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['cats']
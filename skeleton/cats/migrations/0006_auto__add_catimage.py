# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CatImage'
        db.create_table('cats_catimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('cats', ['CatImage'])


    def backwards(self, orm):
        # Deleting model 'CatImage'
        db.delete_table('cats_catimage')


    models = {
        'cats.cat': {
            'Meta': {'object_name': 'Cat'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.TextField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'cats.catimage': {
            'Meta': {'object_name': 'CatImage'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cats.favorite': {
            'Meta': {'unique_together': "(('cat', 'uuid'),)", 'object_name': 'Favorite'},
            'cat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cats.Cat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'cats.flag': {
            'Meta': {'unique_together': "(('cat', 'uuid'),)", 'object_name': 'Flag'},
            'cat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cats.Cat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'cats.like': {
            'Meta': {'unique_together': "(('cat', 'uuid'),)", 'object_name': 'Like'},
            'cat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cats.Cat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['cats']
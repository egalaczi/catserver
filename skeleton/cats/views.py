# Create your views here.
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, View, UpdateView, CreateView
from rest_framework import generics, serializers
from rest_framework.fields import Field
from cats.models import Cat, CatImage

import json
from django.http import HttpResponse

class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    response_class = HttpResponse

    def render_to_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        response_kwargs['content_type'] = 'application/json'
        return self.response_class(
            self.convert_context_to_json(context),
            **response_kwargs
        )

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)


class ActionView(JSONResponseMixin, UpdateView):
    model = Cat

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(ActionView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        return {"message":"OK"}


    def post(self, request, *args, **kwargs):
        try:
            action = self.kwargs['action']
            if action=="like":
                self.get_object().like_set.create(uuid = request.POST['my_username'])
            elif action=="dislike":
                self.get_object().like_set.filter(uuid = request.POST['my_username']).delete()
            elif action=="favorite":
                favorited = self.get_object().favorite_set.filter(uuid = request.POST['my_username']).count()
                if favorited:
                    self.get_object().favorite_set.filter(uuid = request.POST['my_username']).delete()
                else:
                    self.get_object().favorite_set.create(uuid = request.POST['my_username'])
            elif action=="flag":
                flagged = self.get_object().flag_set.filter(uuid = request.POST['my_username']).count()
                if flagged:
                    self.get_object().flag_set.filter(uuid = request.POST['my_username']).delete()
                else:
                    self.get_object().flag_set.create(uuid = request.POST['my_username'])

        except Exception, e:
            print e
        return super(ActionView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(ActionView, self).get(request, *args, **kwargs)



class CatView(generics.ListCreateAPIView):
    model = Cat




class CatSerializer(serializers.ModelSerializer):
    next = Field('next_obj')
    prev = Field('prev_obj')
    likes = Field('likes')
    favorites = Field('favorites')
    liked = serializers.SerializerMethodField('get_liked')
    favorited = serializers.SerializerMethodField('get_favorited')
    flagged = serializers.SerializerMethodField('get_flagged')
    class Meta:
        model = Cat

    def get_liked(self, obj):

        uuid = self.context['request'].META.get('HTTP_X_USER_TOKEN2',None)
        if uuid:
            return obj.like_set.filter(uuid=uuid).count()>0
        return False

    def get_favorited(self, obj):

        uuid = self.context['request'].META.get('HTTP_X_USER_TOKEN2',None)
        if uuid:
            return obj.favorite_set.filter(uuid=uuid).count()>0
        return False


    def get_flagged(self, obj):

        uuid = self.context['request'].META.get('HTTP_X_USER_TOKEN2',None)
        if uuid:
            return obj.flag_set.filter(uuid=uuid).count()>0
        return False



class CatDetailView(generics.RetrieveUpdateDestroyAPIView):
    model = Cat
    serializer_class = CatSerializer

@csrf_exempt
def upload(request):
    try:
        file = request.FILES['file']
        catimage = CatImage(file=file)
        catimage.save()

        cat = Cat()
        cat.image = "http://localhost:8000/"+catimage.file.url
        cat.description = request.POST.get('description',"some cat")
        cat.save()

        return HttpResponse(str(cat.id))
    except Exception, e:
        print e
        raise e

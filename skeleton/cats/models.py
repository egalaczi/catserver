from django.db import models

class CatImage(models.Model):
    file = models.FileField(upload_to="static/img/cats")

# Create your models here.
class Cat(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    description = models.TextField()
    image = models.TextField()

    def next_obj(self):
        try:
            return self.get_next_by_timestamp().id
        except:
            return Cat.objects.all()[0].id
    def prev_obj(self):
        try:
            return self.get_previous_by_timestamp().id
        except:
            return Cat.objects.all().latest('pk').id

    def likes(self):
        return self.like_set.all().count()


    def favorites(self):
        return self.favorite_set.all().count()

class RelatedBase(models.Model):
    cat = models.ForeignKey(Cat)
    uuid = models.CharField(max_length=50)
    class Meta:
        abstract = True
        unique_together = ('cat','uuid',)

class Like(RelatedBase):
    pass

class Favorite(RelatedBase):
    pass

class Flag(RelatedBase):
    pass
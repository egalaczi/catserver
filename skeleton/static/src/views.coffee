define ->
  class TodoView extends Backbone.View
    events:
      "click .pyc": "removeItem"
      "click .text": "modifyItem"
    initialize: =>
      @model.bind 'change', @render
      @model.bind 'destroy', @unrender
    removeItem: =>
      @model.destroy()
    modifyItem: =>
      new_value = window.prompt "sometext","defaultvalue"
      @model.set 'description', new_value
      @model.save()
    render: =>
      $(@el).html """
      <p><span class='text'>#{@model.get 'id'}:#{@model.get 'description'}</span>
        <span class='delete'>x</span></p>"""
      @
    unrender: =>
      @el.remove()